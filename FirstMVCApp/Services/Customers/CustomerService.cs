﻿using FirstMVCApp.DAL;
using FirstMVCApp.Services.Customers.Contacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models.Customers;

namespace FirstMVCApp.Services.Customers
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CustomerService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new 
                    ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void CreateCustomer(CustomerCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var customer = Mapper.Map<Customer>(model);
                unitOfWork.Customers.Create(customer);
            }
        }

        public List<CustomerModel> GetCustomersList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var customers = unitOfWork.Customers.GetAll();
                return Mapper.Map<List<CustomerModel>>(customers);
            }
        }
    }

}
