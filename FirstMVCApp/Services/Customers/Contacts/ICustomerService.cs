﻿using FirstMVCApp.Models.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Services.Customers.Contacts
{
    public interface ICustomerService
    {
        void CreateCustomer(CustomerCreateModel model);
        List<CustomerModel> GetCustomersList();
    }
}
