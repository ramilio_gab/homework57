﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Models.Orders
{
    public class OrderIndexModel
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public DateTime OrderCreatedOn { get; set; }
        public string CustomerName { get; set; }
        public string OrderSum { get; set; }
    }
}
