﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.Models.Customers;
using FirstMVCApp.Services.Customers.Contacts;
using FirstMVCApp.Services.Orders.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;


        public CustomerController(ICustomerService customerService, IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (customerService == null)
                throw new ArgumentNullException(nameof(customerService));
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _customerService = customerService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IActionResult Index()
        {
            var customerModels = _customerService.GetCustomersList();
            return View(customerModels);
        }
        
         
        

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateCustomer(CustomerCreateModel model)
        {
            try
            {
                _customerService.CreateCustomer(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}