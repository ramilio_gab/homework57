﻿using System;
using FirstMVCApp.Models.Orders;
using FirstMVCApp.Services.Orders.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            if (orderService == null)
                throw new ArgumentNullException(nameof(orderService));

            _orderService = orderService;
        }

        [HttpGet("Order/{productId?}")]
        public IActionResult Create(int? productId)
        {
            if(!productId.HasValue)
                throw new ArgumentOutOfRangeException(nameof(productId));

            var model = _orderService.GetOrderCreateModel(productId.Value);

            return View(model);
        }

        [HttpPost]
        public IActionResult CreateOrder(OrderCreateModel model)
        {
            _orderService.CreateOrder(model);

            return RedirectToAction("Index", "Product");
        }

        [HttpGet("Orders")]
        public IActionResult Index()
        {
            var orders = _orderService.GetOrdersList();

            return View(orders);
        }
    }
}
