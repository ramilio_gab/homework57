﻿using System.Collections.Generic;
using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories.Contracts
{
    public interface IRepository<T> where T : Entity
    {
        T Create(T entity);

        T GetById(int id);

        IEnumerable<T> GetAll();

        T Update(T entity);

        void Remove(T entity);
    }
}
