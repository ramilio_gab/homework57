﻿using System.Collections.Generic;
using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories.Contracts
{
    public interface IOrderRepository : IRepository<Order>
    {
        IEnumerable<Order> GetAllWithProduct();
    }
}