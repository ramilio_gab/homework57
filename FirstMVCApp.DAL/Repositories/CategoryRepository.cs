﻿using FirstMVCApp.DAL.Entities;
using FirstMVCApp.DAL.Repositories.Contracts;

namespace FirstMVCApp.DAL.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Categories;
        }
    }
}