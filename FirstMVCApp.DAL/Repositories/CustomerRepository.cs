﻿using FirstMVCApp.DAL.Entities;
using FirstMVCApp.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCApp.DAL.Repositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
                
    {
        public CustomerRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Customers;
        }
    }
}
