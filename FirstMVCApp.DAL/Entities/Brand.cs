﻿using System.Collections.Generic;

namespace FirstMVCApp.DAL.Entities
{
    public class Brand : Entity
    {
        public string Name { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
