﻿using FirstMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCApp.DAL.EntitiesConfiguration
{
    public class CustomerConfiguration : BaseEntityConfiguration<Customer>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Customer> builder)
        {
            builder
                .Property(c => c.Name)
                .HasMaxLength(250)
                .IsRequired();
            builder
                .Property(c => c.Mail)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .HasIndex(c => c.Name)
                .IsUnique();
        }
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Customer> builder)
        {
            builder
                .HasMany(p => p.Orders)
                .WithOne(p => p.Customer)
                .HasForeignKey(p => p.CustomerId);
        }
    }

}
