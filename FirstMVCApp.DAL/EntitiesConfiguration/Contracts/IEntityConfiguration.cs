﻿using System;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.DAL.EntitiesConfiguration.Contracts;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCApp.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : Entity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
} 